<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <title>HTML5 basic skeleton</title>
    <link rel="stylesheet" href="style.css">
 	<script src="http://code.jquery.com/jquery-latest.min.js"></script>
	 </head>
  <body>
    <div id="wrapper">
      <header>Banière ou en-tête du contenu</header>
      <nav><ul><li>Menu de navigation</li></ul></nav>
      <section id="content">
        <article>Premier article</article>
      </section>
      <aside>Barre sur le côté droit</aside>
      <footer>Pied de page du contenu</footer>
    </div>
  </body>
</html>