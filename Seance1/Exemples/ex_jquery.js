\begin{lstlisting}
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title> Sélection par identifiant </title>
    <style>
      #p1, #p3 {color: red;}
    </style>
  </head>
  <body>
    <p id="p1">paragraphe 1</p>
    <p id="p2">paragraphe 2</p>
    <div id="p3">paragraphe 3</div>
    <div id="p4">paragraphe 4</div>
  </body>
</html>
			\end{lstlisting}