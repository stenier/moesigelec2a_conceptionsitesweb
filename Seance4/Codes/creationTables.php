<!DOCTYPE html>
<html><head><title>Création de la table dans la BDD</title><meta charset="utf-8"></head>
	<body><h1>Exécution du script de création </h1>
<?php
	require_once("configBDD.inc.php");
	$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
	if ($mysqli->connect_errno) {
		echo "Echec lors de la connexion à MySQL : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}else{
		$res=$mysqli->query("CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(32) NOT NULL,
  `prenom` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
		if(!$res){
			die("erreur dans la requête");
		}else{
			echo "<p>table créée</p>";
		}
	}
?></body></html>
